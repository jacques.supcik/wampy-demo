import random
import time

from wampy.peers.clients import Client
from wampy.peers.routers import Crossbar

with Client(router=Crossbar()) as client:
    while True:
        client.publish(topic="live-data", message={'battery': random.randint(50, 100)})
        time.sleep(1)