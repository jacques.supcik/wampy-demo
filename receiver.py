from wampy.peers.clients import Client
from wampy.peers.routers import Crossbar
from wampy.roles.subscriber import subscribe


class WampyApp(Client):

    @subscribe(topic="live-data")
    def event(self, message=None, meta=None):
        print(message)

with WampyApp(router=Crossbar()) as client:
    client.start()
    while True:
        client.session._managed_thread.wait()
